import { LotteryService } from './service/lottery.service';
import { Web3Service } from './service/web3.service';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

const BigNumber = require("big-number");
declare let require: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  
  public manager: any = '';
  public players: [any];
  public accounts: [any];
  public balance: any = '';
  public lotteryForm : FormGroup;
  public message: string;

  constructor(
    private web3Service: Web3Service,
    private lotteryService: LotteryService
  ) { }

  ngOnInit(){
    this.initForm();
    this.web3Functions();
  }

  async web3Functions() {
    this.web3Service._web3.eth.getAccounts().then(console.log);
    this.manager = await this.lotteryService._istance.methods.manager().call();
    this.players = await this.lotteryService._istance.methods.getPlayers().call();
    this.balance = await this.web3Service._web3.eth.getBalance(this.lotteryService._istance.options.address);
    
    console.log("MANAGER ",this.manager);
    console.log("PLAYERS ",this.players);
    console.log("BALANCE ",this.balance);
    // console.log("From Web3Js",this.value);
  }

  initForm(){
    this.lotteryForm = new FormGroup({
      'etherAmount' : new FormControl('', Validators.required)
    });
  }

  async onCreate() {
    console.log(this.lotteryForm.value);
    this.accounts = await this.web3Service._web3.eth.getAccounts();
    console.log("Accounts ",this.accounts);

    console.log(typeof this.lotteryForm.value.etherAmount);

    this.message = "Waiting For the Transaction To get Completed......";

    await this.lotteryService._istance.methods.enter().send({
      from: this.accounts[0],
      value: this.web3Service._web3.utils.toWei(this.lotteryForm.value.etherAmount, 'ether')
    });

    this.message = "You have Entered:)";
  }

  async pickWinner() {
    this.accounts = await this.web3Service._web3.eth.getAccounts();
    this.message = "Waiting For the Transaction To get Completed......";
    await this.lotteryService._istance.methods.pickWinner().send({
      from: this.accounts[0]
    });

    this.message = "Winner Picked";
  }
}
